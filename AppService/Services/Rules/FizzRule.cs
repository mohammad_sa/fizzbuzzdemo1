﻿using AppServices.Interface.IFizzBuzzRule;
using AppServices.Service.Constant;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppServices.Service.Rules
{
    public class FizzRule : IRule
    {
        public FizzRule()
        {

        }
        public bool IsNumberMatched(int enteredNumber)

        {
            return enteredNumber % 3 == 0 && enteredNumber % 5 != 0;
        }

        public string GetReplacedWord(string dayWeek)
        {
            return (dayWeek == Constants.DayWeekRule) ? "Wizz" : "Fizz";
        }
    }
}
