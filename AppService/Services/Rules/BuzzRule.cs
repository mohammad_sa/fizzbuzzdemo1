﻿using AppServices.Interface.IFizzBuzzRule;
using AppServices.Service.Constant;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppServices.Service.Rules
{
    public class BuzzRule : IRule
    {
        public BuzzRule()
        {

        }
        public bool IsNumberMatched(int enteredNumber)
        {
            return enteredNumber % 5 == 0 && enteredNumber % 3 != 0;
        }

        public string GetReplacedWord(string dayWeek)
        {
            return (dayWeek == Constants.DayWeekRule) ? "Wuzz" : "Buzz";
        }
    }
}
