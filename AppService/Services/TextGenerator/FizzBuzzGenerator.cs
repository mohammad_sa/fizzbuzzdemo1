﻿using System;
using System.Collections.Generic;
using System.Text;
using AppServices.Interface.IFizzBuzzRule;
using AppServices.Service.Rules;

namespace AppServices.Interface
{
    public class FizzBuzzGenerator : IFizzBuzzGenerator
    {

        private readonly IEnumerable<IRule> _Rules;

        public FizzBuzzGenerator(IEnumerable<IRule> Rules)
        {
            _Rules = Rules;
        }
        //private List<IRule> _Rules;
        //public FizzBuzzGenerator()
        //{
        //    LoadRules();
        //    //_iFizzRule = iFizzRule;
        //}
        //public void LoadRules()
        //{
        //    this._Rules = new List<IRule>
        //    {
        //        new FizzRule(),
        //        new BuzzRule(),
        //        new FizzBuzzRule()
        //    };
        //}

        public List<string> GetMessages(int enteredNumber, string dayOftheWeek)
        {
            List<string> messages = new List<string>();

            for (int count = 1; count <= enteredNumber; count++)
            {
                messages.Add(ProcessFizzBuzzData(count, dayOftheWeek));
            }
            return messages;
        }

        public string ProcessFizzBuzzData(int sequenceNumber, string dayOftheWeek)
        {
            foreach (var rule in _Rules)
            {
                if (rule.IsNumberMatched(sequenceNumber))
                {
                    return rule.GetReplacedWord(dayOftheWeek);
                }
            }
            return Convert.ToString(sequenceNumber);
        }
    }
}
