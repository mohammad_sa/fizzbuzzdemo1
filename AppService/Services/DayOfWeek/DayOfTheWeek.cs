﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppServices.Service.DayOfTheWeek
{
    public class DayOfTheWeek : IDayOfTheWeek
    {
        public DayOfWeek GetCurrentDay()
        {
            return DateTime.Today.DayOfWeek;
        }
    }
}
