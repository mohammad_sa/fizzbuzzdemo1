﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppServices.Interface
{
    public interface IFizzBuzzGenerator
    {
        public List<string> GetMessages(int enteredNumber, string dayOfTheWeek);
    }
}
