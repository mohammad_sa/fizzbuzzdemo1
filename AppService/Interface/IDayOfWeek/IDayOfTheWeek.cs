﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppServices.Service.DayOfTheWeek
{
    public interface IDayOfTheWeek
    {
        DayOfWeek GetCurrentDay();
    }
}
