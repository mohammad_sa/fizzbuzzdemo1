﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppServices.Interface.IFizzBuzzRule
{
    public interface IRule
    {
        bool IsNumberMatched(int enteredNum);
        string GetReplacedWord(string dayWeek);
    }
}
