﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AppServices.Interface;
using AppServices.Service.DayOfTheWeek;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFizzBuzzGenerator _messages;
        private readonly IDayOfTheWeek _dayOfTheWeek;
        private readonly IConfiguration _configuration;

        public HomeController(IFizzBuzzGenerator messages, IDayOfTheWeek dayOfTheWeek, IConfiguration iConfig)
        {
            _messages = messages;
            _dayOfTheWeek = dayOfTheWeek;
            _configuration = iConfig;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(IndexModel model, int currentPageIndex = 1)
        {
            if (ModelState.IsValid)
            {
                int maxRows = Convert.ToInt32(_configuration.GetValue<string>("ApplicationConstant:maxAllowedRows"));
                List<string> lstMessages = new List<string>();
                IndexModel processed = new IndexModel();
                int inputData = (int)model.InputData;
                lstMessages = _messages.GetMessages(inputData, _dayOfTheWeek.GetCurrentDay().ToString());

                processed.Messages = lstMessages.Skip((currentPageIndex - 1) * maxRows).Take(maxRows).ToList();
                processed.PageCount = (int)Math.Ceiling((double)(lstMessages.Count() / Convert.ToDecimal(maxRows)));
                processed.CurrentPageIndex = currentPageIndex;
                processed.InputData = inputData;

                //IPagedList<ResultModel.> abc = processed.OutputResult.ToPagedList(1,4);

                return View(processed);

            }
            else
            {
                return View();
            }
        }
    }
}

